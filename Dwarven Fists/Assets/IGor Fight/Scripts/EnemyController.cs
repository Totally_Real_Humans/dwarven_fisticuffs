﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour {

	private Animator animator;
		
	public CircleCollider2D rightFist;
	public CircleCollider2D leftFist;
	
	public float frameTime;
	public float frameTimeInterval;
	public float attackInterval;
	public float attackTime;
	public float minAttack;
	public float maxAttack;
	public float blockTime;
	
	public Slider enemyHealth;
	public bool isDamaged = false;
	public bool isDamagedByHeadbutt = false;
	public int alternateAttack = 1;
	public int punchTime = 1;
	public int combo = 0;
	public bool attackOn = false;
	public int attackPicker;
	
	private float hitReset = 0.0f;
	public float invulnerability;
	
	public bool invulnerable = false;
	public bool iLaser = false;
	public bool blocking = false;
	public bool isDefeated = false;
	public bool rocketTime = false;
	
	public AudioSource punchSound;
	public AudioSource blockSound;
	
	GameObject[] pauseObjects;
	
	public Text damagePopupText;
	
	public float animSpeed;
	public static int enemyStrength = 10;
	
	public CameraShake cameraShake;
	
	public float damageTaken;
	public iGorEvents iGorEvents;
		
	public float healthBar3;
	
	public float healthBar1Value;
	public float healthBar2Value;
	public float healthBar3Value;
	
	public bool phase1Complete;
	public bool phase2Complete;
	
	public Image iGorHealthBar1;
	public Image iGorHealthBar2;
	public Image iGorHealthBar3;
	
	// Use this for initialization
	void Start () {
		attackTime = Time.fixedTime + Random.Range(minAttack, maxAttack) + 2.0f;
		animator = GetComponent<Animator>();
		pauseObjects = GameObject.FindGameObjectsWithTag("Enemy");
		iGorEvents = this.GetComponent<iGorEvents>();
		GameObject camera = GameObject.Find("Main Camera");
		cameraShake = camera.GetComponent<CameraShake>();
		//Setting Speed of Animator;
		animator.speed = animSpeed;
	}
	
	// Update is called once per frame
	void Update () 
	{
		turnOffWasCountered();
		iGorTakesAHit();
		attack();
		//testAttacks();
		healthLeft();
		if (attackOn == false)
		{
			combo = 0;
		}
		if (enemyHealth.value <= enemyHealth.maxValue/3 && iLaser == false){
			phase2Complete = true;
			iGorHealthBar2.enabled = false;
			iLaser = true;
			Invoke("iLaserRight", 1);
		}
		if (enemyHealth.value <= enemyHealth.maxValue * 2/3 && rocketTime == false){
			phase1Complete = true;
			iGorHealthBar3.enabled = false;
			rocketTime = true;
			Invoke("rocketFists", 1);
		}
		damagePopupText.text = damageTaken.ToString();
	}
	
	void attack()
	{
		if (attackTime < Time.fixedTime){
			if (blocking == true)
			{
				resetTriggers();
				animator.SetTrigger("iGorIdle");
			}
			attackOn = true;
			
		}
		if (attackOn == true)
		{
			resetCounter();
			attackPicker = Random.Range(0, 2);
			if (alternateAttack == 1 && attackPicker == 0){
				leftLongJab();
			}
			else if (alternateAttack == 1 && attackPicker == 1){
				rightLongJab();
			}
			else if (alternateAttack == 2 && attackPicker == 0){
				leftQuickJab();
			}
			else if (alternateAttack == 2 && attackPicker == 1){
				rightQuickJab();
			}
			else if (alternateAttack == 3 && attackPicker == 0){
				iLaserLeft();
			}
			else if (alternateAttack == 3 && attackPicker == 1){
				iLaserRight();
			}
			else if (alternateAttack == 4 && attackPicker == 0){
				rocketFists();
			}
		}
	}
	
	void testAttacks(){
		attackOn = true;
		if (Input.GetKeyDown("1")){
			resetTriggers();
			enemyStrength = 10;
			animator.SetTrigger("iGorLeftPunch");
		}
		if (Input.GetKeyDown("2")){
			resetTriggers();
			enemyStrength = 10;
			animator.SetTrigger("iGorRightPunch");
		}
		if (Input.GetKeyDown("3")){
			resetTriggers();
			enemyStrength = 20;
			animator.SetTrigger("iGorLeftHook");
		}
		if (Input.GetKeyDown("4")){
			resetTriggers();
			enemyStrength = 20;
			animator.SetTrigger("iGorRightHook");
		}
		if (Input.GetKeyDown("5")){
			resetTriggers();
			enemyStrength = 30;
			animator.SetTrigger("iGoriLaser");
		}
		if (Input.GetKeyDown("6")){
			resetTriggers();
			enemyStrength = 30;
			animator.SetTrigger("iGorLeftLaser");
		}
		if (Input.GetKeyDown("7")){
			resetTriggers();
			enemyStrength = 10;
			animator.SetTrigger("iGorRocketFists");
			int randomRocketFists = Random.Range(1, 4);
			Debug.Log(randomRocketFists);
			if (randomRocketFists == 1){
				animator.SetTrigger("iGorRainingFists");
			}
			if (randomRocketFists == 2){
				animator.SetTrigger("iGorRainingFists2");
			}
			if (randomRocketFists == 3){
				animator.SetTrigger("iGorRainingFists3");
			}
		}
	}
	
	void OnTriggerEnter2D(Collider2D coll) 
	{
		if (coll.gameObject.tag == "Fist" && hitReset < Time.fixedTime && invulnerable == false)
		{
			isDamaged = true;
			disableFists();
			attackTime += blockTime;
		}
		else if ((coll.gameObject.tag == "Fist" || coll.gameObject.tag == "Headbutt") && hitReset < Time.fixedTime && invulnerable == true && blocking == true)
		{
			resetTriggers();
			blockSound.Play();
			animator.SetTrigger("iGorCounter");
			if (enemyHealth.value <= enemyHealth.maxValue/3){
				animator.SetTrigger("iGorCounterPunch");
				animator.SetTrigger("iGorCounterPunch2");
			}
			else if (enemyHealth.value <= enemyHealth.maxValue * 2/3){
				animator.SetTrigger("iGorCounterPunch");
			}
		}
		else if (coll.gameObject.tag == "Headbutt" && hitReset < Time.fixedTime && invulnerable == false){
			isDamaged = true;
			isDamagedByHeadbutt = true;
			disableFists();
			attackTime += blockTime;
		}
	}

	void rightLongJab()
	{
		iGorEvents.textOff(4);
		enemyStrength = 10;
		if (combo < 1)
		{
			if (punchTime == 1 && frameTime < Time.fixedTime)
			{
				resetTriggers();
				animator.SetTrigger("iGorRightPunch");
			}
			rightPunch();
		}	
		
		else
		{
			attackOn = false;
			attackTime = Time.fixedTime + Random.Range(minAttack, maxAttack);
			alternateAttack = 2;
		}
	}
	
	void leftLongJab(){
		iGorEvents.textOff(4);
		enemyStrength = 10;
		if (combo < 1){
			if (punchTime == 1 && frameTime < Time.fixedTime)
			{
				resetTriggers();
				animator.SetTrigger("iGorLeftPunch");
			}
			leftPunch();
		}
		else
		{
			attackOn = false;
			attackTime = Time.fixedTime + Random.Range(minAttack, maxAttack);
			alternateAttack = 2;
		}
	}
	
	void leftQuickJab(){
		iGorEvents.textOff(4);
		enemyStrength = 10;
		if (combo < 1){
			if (punchTime == 1 && frameTime < Time.fixedTime)
			{
				resetTriggers();
				animator.SetTrigger("iGorLeftHook");
			}
			leftPunch();
		}
		else{
			attackOn = false;
			attackTime = Time.fixedTime + Random.Range(minAttack, maxAttack);
			if (iLaser == true){
				alternateAttack = 3;
			}
			else{
				alternateAttack = 1;
			}
		}
	}
	
	void rightQuickJab(){
		iGorEvents.textOff(4);
		enemyStrength = 10;
		if (combo < 1){
			if (punchTime == 1 && frameTime < Time.fixedTime)
			{
				resetTriggers();
				animator.SetTrigger("iGorRightHook");
			}
			rightPunch();
		}
		else{
			attackOn = false;
			attackTime = Time.fixedTime + Random.Range(minAttack, maxAttack);
			if (iLaser == true){
				alternateAttack = 3;
			}
			if (rocketTime == true && iLaser == false){
				alternateAttack = 4;
			}
			else{
				alternateAttack = 1;
			}
		}
	}
	
	
	void rightPunch(){
		iGorEvents.textOff(4);
		enemyStrength = 10;
		if (punchTime == 1 && frameTime < Time.fixedTime)
		{
			frameTime = Time.fixedTime + attackInterval;
			punchTime++;
		}
		else if (punchTime == 2 && frameTime < Time.fixedTime)
		{
			frameTime = Time.fixedTime + attackInterval;
			punchTime++;
		}
		else if (punchTime == 3 && frameTime < Time.fixedTime)
		{
			frameTime = Time.fixedTime + attackInterval;
			punchTime++;
		}
		else if (punchTime == 4 && frameTime < Time.fixedTime)
		{
			frameTime = Time.fixedTime + attackInterval;
			//rightFist.enabled = true;
			punchTime++;
		}
		else if (frameTime < Time.fixedTime)
		{
			frameTime = Time.fixedTime + attackInterval;
			punchTime = 1;
			combo++;
		}
	}
	
	void leftPunch(){
		iGorEvents.textOff(4);
		enemyStrength = 10;
		if (punchTime == 1 && frameTime < Time.fixedTime)
		{
			frameTime = Time.fixedTime + attackInterval;
			punchTime++;
		}
		else if (punchTime == 2 && frameTime < Time.fixedTime)
		{
			frameTime = Time.fixedTime + attackInterval;
			punchTime++;
		}
		else if (punchTime == 3 && frameTime < Time.fixedTime)
		{
			frameTime = Time.fixedTime + attackInterval;
			punchTime++;
		}
		else if (punchTime == 4 && frameTime < Time.fixedTime)
		{
			frameTime = Time.fixedTime + attackInterval;
			//leftFist.enabled = true;
			punchTime++;
		}
		else if (frameTime < Time.fixedTime)
		{
			frameTime = Time.fixedTime + attackInterval;
			punchTime = 1;
			combo++;
		}
	}

	void healthLeft()
	{
		if (enemyHealth.value <= 0)
		{
			defeatConditions();
		}
	}
	
	void iGorTakesAHit()
	{
		if(isDamaged == true)
		{
			resetTriggers();
			damageTaken = WorldController.control.strength;
			iGorEvents.textOn(4);
			animator.SetTrigger("iGorHit");
			punchSound.Play();
			healthBar3 = enemyHealth.maxValue/3;
			enemyHealth.value = enemyHealth.value - WorldController.control.strength;	
			if (isDamagedByHeadbutt == true){
				enemyHealth.value = enemyHealth.value - WorldController.control.strength;
				damageTaken = WorldController.control.strength + damageTaken;
			}
			if (attackOn == true)
			{
				animator.SetBool("iGorWasCountered", true);
				cameraShake.shakeDuration = 0.1f;
				enemyHealth.value = enemyHealth.value - WorldController.control.strength;
				damageTaken = WorldController.control.strength + damageTaken;
			}
			if (phase2Complete == true && phase1Complete == true){
				healthBar1Value += damageTaken;
				iGorHealthBar1.fillAmount = (healthBar3 - healthBar1Value)/healthBar3;
			}
			else if (phase1Complete == true){
				healthBar2Value += damageTaken;
				iGorHealthBar2.fillAmount = (healthBar3 - healthBar2Value)/healthBar3;
			}
			else{
				healthBar3Value += damageTaken;
				iGorHealthBar3.fillAmount = (healthBar3 - healthBar3Value)/healthBar3;
			}
			Debug.Log(healthBar3Value);
			isDamaged = false;
			isDamagedByHeadbutt = false;
			hitReset = Time.fixedTime + invulnerability;
		}
	}
	
	void iLaserRight(){
			iGorEvents.textOff(4);
			resetTriggers();
			enemyStrength = 30;
			animator.SetTrigger("iGoriLaser");
			disableFists();
			attackOn = false;
			attackTime = Time.fixedTime + Random.Range(minAttack, maxAttack) + 3.3f;
			alternateAttack = 1;
	}
	
	void iLaserLeft(){
			iGorEvents.textOff(4);
			resetTriggers();
			enemyStrength = 30;
			animator.SetTrigger("iGorLeftLaser");
			disableFists();
			attackOn = false;
			attackTime = Time.fixedTime + Random.Range(minAttack, maxAttack) + 3.3f;
			alternateAttack = 1;
	}
	
	void rocketFists(){
		iGorEvents.textOff(4);
		enemyStrength = 10;
		resetTriggers();
		animator.SetTrigger("iGorRocketFists");
		int randomRocketFists = Random.Range(1, 4);
		if (randomRocketFists == 1){
			animator.SetTrigger("iGorRainingFists");
		}
		if (randomRocketFists == 2){
			animator.SetTrigger("iGorRainingFists2");
		}
		if (randomRocketFists == 3){
			animator.SetTrigger("iGorRainingFists3");
		}
		attackOn = false;
		attackTime = Time.fixedTime + Random.Range(minAttack, maxAttack) + 6.0f;
		alternateAttack = 1;
	}
	
	void defeatConditions()
	{
		resetTriggers();
		iGorEvents.textOff(4);
		animator.SetTrigger("iGorDeath");
		foreach (GameObject obj in pauseObjects){
				//obj.SetActive(false);
		}
		isDefeated = true;
	}
	
	public bool isDefeatedQuestion()
	{
		return isDefeated;
	}
	
	void disableFists()
	{
		rightFist.enabled = false;
		leftFist.enabled = false;
	}
	
	public void resetCounter()
	{
		animator.ResetTrigger("iGorCounter");
	}
	
	void turnOffWasCountered()
	{
		if (attackOn == false)
		{
			animator.SetBool("iGorWasCountered", false);
		}
	}
	
	void resetTriggers(){
		animator.ResetTrigger("iGorRightPunch");
		animator.ResetTrigger("iGorRightPunchExe");
		animator.ResetTrigger("iGorLeftPunch");
		animator.ResetTrigger("iGorRightPunchExe");
		animator.ResetTrigger("iGorHit");
		animator.ResetTrigger("iGorSurprised");
		animator.ResetTrigger("iGorLeftHook");
		animator.ResetTrigger("iGoriLaser");
		animator.ResetTrigger("iGorRightHook");
		animator.ResetTrigger("iGorLeftLaser");
		animator.ResetTrigger("iGorDeath");
		animator.ResetTrigger("iGorCounter");
		animator.ResetTrigger("iGorLeftPunch");
		animator.ResetTrigger("iGorLeftPunchExe");
		animator.ResetTrigger("iGorHit");
	}
}
