﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryOrDefeatMoonlock : MonoBehaviour 
{
	public PlayerController player;
	public MoonlockController enemy;
	
	public string lossScene;
	public string winScene;
	
	public int lossProgress;
	public int winProgress;
	public string enemyName;
	public float delaySceneChange;
	
	private float sceneTime;
	private bool timeChanged = false;
	
	void Update () 
	{
		if (player.isDefeatedQuestion() == true)
		{
			Win();
		}
		
		if (enemy.isDefeatedQuestion() == true)
		{
			Loss();
		}
	}
	
	void Win()
	{
		setFightData();
		PlayerPrefs.SetInt("Win", 0);
		runTime();
		if (sceneTime < Time.fixedTime)
		{
			 if (WorldController.control.progress[winProgress] == true)
			 {
				 SceneManager.LoadScene("Result Screen");
			 }
			 else
			 {
				 SceneManager.LoadScene(winScene);
			 }
		}
	}
	
	void Loss()
	{
		setFightData();
		PlayerPrefs.SetInt("Win", 1);
		runTime();
		if (sceneTime < Time.fixedTime)
		 {
			 if (WorldController.control.progress[lossProgress] == true)
			 {
				 SceneManager.LoadScene("Result Screen");
			 }
			 else
			 {
				 SceneManager.LoadScene(lossScene);
			 }
		}
	}
	
	void setFightData()
	{
		PlayerPrefs.SetString("Enemy", enemyName);
	}
	
	void runTime()
	{
		if (timeChanged == false)
		{
			sceneTime = Time.fixedTime + delaySceneChange;
			timeChanged = true;
		}
	}
}
