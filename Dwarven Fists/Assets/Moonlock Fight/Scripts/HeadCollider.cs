﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadCollider : MonoBehaviour {
	
	bool isHit;
	bool isDamagedByHeadbutt;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnTriggerEnter2D(Collider2D coll) 
	{
		if (coll.gameObject.tag == "Fist")
		{
			isHit = true;
		}
		else if (coll.gameObject.tag == "Headbutt"){
			isHit = true;
			isDamagedByHeadbutt = true;
			Debug.Log(isDamagedByHeadbutt);
		}
	}
	
	public bool getIsHit(){
		return isHit;
	}
	
	public void setIsHit(bool isHit){
		this.isHit = isHit;
	}

	public bool getIsDamagedByHeadbutt(){
		return isDamagedByHeadbutt;
	}
	
	public void setIsDamagedByHeadbutt(bool isDamagedByHeadbutt){
		this.isDamagedByHeadbutt = isDamagedByHeadbutt;
	}
}
