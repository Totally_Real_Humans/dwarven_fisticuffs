﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoonlockController : MonoBehaviour {

	private Animator animator;
	public Animation anim;
	
	public Slider moonlockHealth;
	public AudioSource punchSound;
	public bool isDamaged = false;
	private float hitReset = 0.0f;
	public float invulnerability;
	private Vector3 lastPosition;
	public float animatorTime = 0.0f;
	public float maxSpeed = 0.0f;
	public float speedUp = 0.25f;
	public bool isDamagedByHeadbutt;
	public bool dazed;
	private GameObject[] pauseObjects;
	public bool isDefeated = false;
	
	public float minAttack;
	public float maxAttack;
	public GameObject leftGlove;

	public float attackTime;
	public int attackPicker;
	public int alternateAttack = 1;
	public bool attackOn = false;
	
	public Image moon;
	public Sprite reverseMoon;
	public Sprite crescentMoon;
	public float moonTransparency;
	public bool isFullMoon = false;
	public bool isNewMoon = false;
	public bool invulnerable = false;
	public static bool isCountered = false;
	
	public HeadCollider headCollider;
	
	public int attackPower;

	public float healthBar3;
	public float damageTaken;
	
	public bool phase1Complete;
	public bool phase2Complete;
		
	public float healthBar1Value;
	public float healthBar2Value;
	public float healthBar3Value;
	
	public Image moonlockHealthBar1;
	public Image moonlockHealthBar2;
	public Image moonlockHealthBar3;
	
	
	// Use this for initialization
	void Start () {
		attackTime = Time.fixedTime + Random.Range(minAttack, maxAttack) + 2.0f;
		animator = GetComponent<Animator>();
		pauseObjects = GameObject.FindGameObjectsWithTag("Enemy");
	}
	
	// Update is called once per frame
	void Update () {
		isDefeatedQuestion();
		moonlockTakesAHit();
		attack();
		//moonlockTestAttacks();
		healthLeft();
		changeMoonImage();
		if (isCountered == true){
			animator.SetTrigger("moonlockCountered");
			isCountered = false;
			isDamaged = true;
		}
		if (moonlockHealth.value <= moonlockHealth.maxValue * 2/3 && isFullMoon == false){
			phase1Complete = true;
			fullMoon();
			isFullMoon = true;
		}
		if (moonlockHealth.value <= moonlockHealth.maxValue/3 && isNewMoon == false){
			phase2Complete = true;
			newMoon();
			isNewMoon = true;
		}
		EnemyController.enemyStrength = attackPower;
		//animator.transform.parent.position = animator.transform.position;
		
	}
	
	
	void moonlockTestAttacks(){
		if (Input.GetKeyDown("1")){
			animator.speed = 1;
			resetTriggers();
			animator.SetTrigger("moonlockRightAttack");
			attackPower = 10;
			Debug.Log(animator.GetCurrentAnimatorClipInfo(0)[0].clip.length);
		}
		if (Input.GetKeyDown("2")){
			animator.speed = 1;
			resetTriggers();
			animator.SetTrigger("moonlockCrescentRight");
			attackPower = 15;
			Debug.Log(animator.GetCurrentAnimatorClipInfo(0)[0].clip.length);
		}
		if (Input.GetKeyDown("3")){
			animator.speed = 1;
			resetTriggers();
			animator.SetTrigger("moonlockNewMoon");
			attackPower = 50;
			Debug.Log(animator.GetCurrentAnimatorClipInfo(0)[0].clip.length);
		}
		if (Input.GetKeyDown("4")){
			animator.speed = 1;
			resetTriggers();
			animator.SetTrigger("moonlockFullMoon");
			attackPower = 10;
			Debug.Log(animator.GetCurrentAnimatorClipInfo(0)[0].clip.length);
		}
		if (Input.GetKeyDown("5")){
			animator.speed = 1;
			resetTriggers();
			animator.SetTrigger("moonlockCrescentLeft");
			attackPower = 15;
			Debug.Log(animator.GetCurrentAnimatorClipInfo(0)[0].clip.length);
		}
	}
	
	void attack()
	{
		if (attackTime < Time.fixedTime){
			attackOn = true;
		}
		
		if (attackOn == true)
		{
			attackPicker = Random.Range(0, 2);
			if (alternateAttack == 1 && attackPicker == 0){
				moonPunch();
			}
			else if (alternateAttack == 1 && attackPicker == 1){
				moonPunch();
			}
			else if (alternateAttack == 2 && attackPicker == 0){
				crescentStrike();
			}
			else if (alternateAttack == 2 && attackPicker == 1){
				crescentStrikeLeft();
			}
			else if (alternateAttack == 3 && attackPicker == 0){
				fullMoon();
			}
			else if (alternateAttack == 3 && attackPicker == 1){
				fullMoon();
			}
			else if (alternateAttack == 4 && attackPicker == 0){
				newMoon();
			}
			else if (alternateAttack == 4 && attackPicker == 1){
				newMoon();
			}
		}
	}
	
	void moonPunch(){
		animator.speed = 1;
		attackPower = 10;
		resetTriggers();
		animator.SetTrigger("moonlockRightAttack");
		attackOn = false;
		attackTime = Time.fixedTime + Random.Range(minAttack, maxAttack) + 1.33f;
		Debug.Log(animator.speed);
		alternateAttack = 2;
	}
	
	void crescentStrike(){
		animator.speed = 1;
		attackPower = 15;
		resetTriggers();
		animator.SetTrigger("moonlockCrescentRight");
		attackOn = false;
		attackTime = Time.fixedTime + Random.Range(minAttack, maxAttack) + 1.9f;
		Debug.Log(animator.speed);
		alternateAttack = 1;
	}
	
	void crescentStrikeLeft(){
		animator.speed = 1;
		attackPower = 15;
		resetTriggers();
		animator.SetTrigger("moonlockCrescentLeft");
		attackOn = false;
		attackTime = Time.fixedTime + Random.Range(minAttack, maxAttack) + 1.9f;
		Debug.Log(animator.speed);
		if (isFullMoon != true){
			alternateAttack = 1;
		}
		else{
			alternateAttack = 3;
		}
	}
	
	void fullMoon(){
		animator.speed = 1;
		attackPower = 10;
		resetTriggers();
		animator.SetTrigger("moonlockFullMoon");
		attackOn = false;
		attackTime = Time.fixedTime + Random.Range(minAttack, maxAttack) + 7.0f;
		Debug.Log(animator.speed);
		if (isNewMoon != true){
			alternateAttack = 1;
		}
		else{
			alternateAttack = 4;
		}
	}
	
	void newMoon(){
		animator.speed = 1;
		attackPower = 50;
		resetTriggers();
		animator.SetTrigger("moonlockNewMoon");
		attackOn = false;
		attackTime = Time.fixedTime + Random.Range(minAttack, maxAttack) + 4.0f;
		Debug.Log(animator.GetCurrentAnimatorClipInfo(0)[0].clip.length);
		alternateAttack = 1;
	}
	
	void OnTriggerEnter2D(Collider2D coll) 
	{
		if (coll.gameObject.tag == "Fist" && hitReset < Time.fixedTime)
		{
			isDamaged = true;
		}
		else if (coll.gameObject.tag == "Headbutt" && hitReset < Time.fixedTime){
			isDamaged = true;
			isDamagedByHeadbutt = true;
		}
	}

	
	void changeMoonImage()
	{
		Color currentColor = moon.color;
		currentColor.a = moonTransparency;
		moon.color = currentColor; 
	}
	
	void reverseMoonImage()
	{
		moon.sprite = reverseMoon;		
	}
	
	void fixMoonImage(){
		moon.sprite = crescentMoon;
	}
	
	void moonlockTakesAHit(){
		if(headCollider.getIsHit() == true && invulnerable != true)
		{	
			damageTaken = WorldController.control.strength;
			healthBar3 = moonlockHealth.maxValue/3;
			headCollider.setIsHit(false);
			if (headCollider.getIsDamagedByHeadbutt() == true){
				moonlockHealth.value = moonlockHealth.value - WorldController.control.strength;
				headCollider.setIsDamagedByHeadbutt(false);
				if (dazed != true){
					damageTaken += WorldController.control.strength;
					resetTriggers();
					animator.SetTrigger("isHeadbutted");
				}
			}
			else{
				if (dazed != true){
					resetTriggers();
					animator.SetTrigger("moonlockHit");
				}
			}
			if (phase2Complete == true && phase1Complete == true){
				healthBar1Value += damageTaken;
				moonlockHealthBar1.fillAmount = (healthBar3 - healthBar1Value)/healthBar3;
			}
			else if (phase1Complete == true){
				healthBar2Value += damageTaken;
				moonlockHealthBar2.fillAmount = (healthBar3 - healthBar2Value)/healthBar3;
			}
			else{
				healthBar3Value += damageTaken;
				moonlockHealthBar3.fillAmount = (healthBar3 - healthBar3Value)/healthBar3;
			}
			isDamaged = false;
			if (animator.speed < maxSpeed && dazed != true){
				//animator.speed += speedUp;
			}
			Debug.Log(healthBar3Value);
			punchSound.Play();
			moonlockHealth.value = moonlockHealth.value - WorldController.control.strength;
			
			hitReset = Time.fixedTime + invulnerability;
		}
	}
	
	void resetTriggers(){
		animator.ResetTrigger("moonlockCrescentRight");
		animator.ResetTrigger("moonlockHit");
		animator.ResetTrigger("moonlockNewMoon");
		animator.ResetTrigger("moonlockRightAttack");
		animator.ResetTrigger("moonlockCrescentLeft");
		animator.ResetTrigger("moonlockFullMoon");
	}
	
	void healthLeft()
	{
		if (moonlockHealth.value <= 0)
		{
			defeatConditions();
		}
	}
	
	public bool isDefeatedQuestion()
	{
		return isDefeated;
	}
	
	void defeatConditions()
	{
		resetTriggers();
		//animator.SetTrigger("moonlockDeath");
		foreach (GameObject obj in pauseObjects){
				obj.SetActive(false);
		}
		isDefeated = true;
	}
}
