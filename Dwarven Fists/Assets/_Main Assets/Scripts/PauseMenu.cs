﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {


	public GameObject children;
	public PlayerController playerScript;
	public string thisScene;
	public GameObject enemy;
	
	private Animator enemyBody;
	
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			TurnOnPause();
		}
	}
	
	public void TurnOnPause()
	{
		children.SetActive(true);
		playerScript.enabled = false;
		enemyBody = enemy.GetComponent<Animator>();
		enemyBody.enabled = false;
	}
	
	public void TurnOffPause()
	{
		enemyBody.enabled = true;
		children.SetActive(false);
		playerScript.enabled = true;
	}
	
	public void Reset()
	{
		SceneManager.LoadScene(thisScene);
	}
	
	public void MainMenu()
	{
		SceneManager.LoadScene("Main Menu");
	}
	
	public void ExitGame()
	{
		Application.Quit();
	}
}
