﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour 
{
	public GameObject tutorialHand;
	
	private Animator tutorialAnimation;
	
	void Start()
	{
		tutorialAnimation = GetComponent<Animator>();
		if (WorldController.control.progress[0] == false)
		{
			tutorialAnimation.enabled = true;
			tutorialHand.SetActive(true);
		}
		else
		{
			tutorialAnimation.enabled = false;
		}
	}

	void Tutorial()
	{
		WorldController.control.Save();
	}
	
	public void startBrassRoeFight()
	{
		if (WorldController.control.progress[0] == false)
		{
			SceneManager.LoadScene("Tutorial Fight");
		}
		else if (WorldController.control.progress[1] == true)
		{
			SceneManager.LoadScene("Brass Roe Fight");
		}
		else
		{
			SceneManager.LoadScene("Roe Intro");
		}
	}
	
	public void startMoonlockFight()
	{
		if (WorldController.control.progress[4] == true)
		{
			SceneManager.LoadScene("Moonlock Fight");
		}
		else
		{
			SceneManager.LoadScene("Moonlock Intro");
		}
	}
	
	public void startIGorFight()
	{
		if (WorldController.control.progress[7] == true)
		{
			SceneManager.LoadScene("IGor Fight");
		}
		else
		{
			SceneManager.LoadScene("iGor Intro Scene");
		}
	}
	
	public void startPreviousStory()
	{
		SceneManager.LoadScene("PreviousStory");
	}
}
