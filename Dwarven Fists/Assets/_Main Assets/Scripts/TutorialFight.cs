﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutorialFight : MonoBehaviour 
{

	private Animator animator;
	
	public SpriteRenderer message;
	public SpriteRenderer motion;
	
	public bool tap = false;
	public bool tapped = false;
	
	public bool dodgeLeft = false;
	public bool dodgedLeft = false;
	
	public bool dodgeRight = false;
	public bool dodgedRight = false;
	
	public bool headbutt = false;
	public bool headbutted = false;
	
	private Vector2 touchPos;
	private Vector2 direction;
	private bool touchDone;
	public bool mobile = false;
	public float swipePixelAmount;

	void Start () 
	{
		animator = GetComponent<Animator>();
	}
	
	void Update () 
	{
		if (mobile == true)
		{
			mobileControls();
		}
		else
		{
			computerControls();
		}
		changeAnimation();
	}
	
	void goToFight()
	{
		WorldController.control.progress[0] = true;
		WorldController.control.Save();
		SceneManager.LoadScene("Roe Intro");
	}
	
	void changeAnimation()
	{
		if (tapped == true)
		{
			animator.SetBool("tappedTrue", true);
			
			if (dodgedLeft == true)
			{
				animator.SetBool("dodgedLeftTrue", true);
				if (dodgedRight == true)
				{
					animator.SetBool("dodgedRightTrue", true);
					if (headbutted == true)
					{
						animator.SetBool("headbuttTrue", true);
					}
				}
			}
		}
	}
	
	
	void computerControls()
	{

		if (dodgeLeft == true && Input.GetKeyDown("a") | Input.GetKeyDown("left"))
		{
			dodgedLeft = true;
		}
		else if (dodgeRight == true && Input.GetKeyDown("d") | Input.GetKeyDown("right"))
		{
			dodgedRight = true;
		}
		else if (tap == true && Input.GetKeyDown(KeyCode.Space))
		{
			tapped = true;
		}
		else if (headbutt == true && Input.GetKeyDown("w") | Input.GetKeyDown("up"))
		{
			headbutted = true;
		}
		
	}
	
	void mobileControls()
	{
		if (Input.touchCount > 0)
        {
			Touch touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    touchPos = touch.position;
					direction = touch.position - touchPos;
                    break;

                case TouchPhase.Moved:
                    direction = touch.position - touchPos;
					if (Math.Abs(direction.x) > swipePixelAmount  || direction.y > swipePixelAmount * 2)
					{
						touchDone = true;
					}
                    break;

                case TouchPhase.Ended:
					touchDone = true;
                    break;
            }
        }
		
		if (touchDone == true)
		{
			float headbuttOrDodge = Math.Abs(direction.x) - Math.Abs(direction.y * 2);
			if (direction.y > (swipePixelAmount * 2)  && headbuttOrDodge < 40)
			{
				headbutted = true;
			}
			else if (direction.x < -swipePixelAmount)
			{
				dodgedLeft = true;
			}
			else if (direction.x > swipePixelAmount)
			{
				dodgedRight = true;
			}
			else
			{
				tapped = true;
			}
		}
		
		touchDone = false;
	}
}
