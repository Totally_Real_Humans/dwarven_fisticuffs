﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WorldController : MonoBehaviour {
	
	public static WorldController control;
	
	//Player Data Information.
	public int health;
	public int strength;
	public int experience;
	public bool[] progress;
	
	private Scene currentScene;
	private Scene previousScene;
	private AsyncOperation async;

	void Awake() 
	{
		
		if (control == null)
		{
			DontDestroyOnLoad(gameObject);
			control = this;
		}
		else if (control != this)
		{
			Destroy(gameObject);
		}
		
		this.Load();
		if (progress[0] == false)
		{
			health = 100;
			strength = 10;
			experience = 0;
			progress[0] = true;
			this.Save();
		}
	}	
	
	public void Save()
	{
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");
		PlayerData data = new PlayerData();
		data.setHealth(health);
		data.setStrength(strength);
		data.setExperience(experience);
		data.setProgress(progress);
		
		bf.Serialize(file, data);
		file.Close();
	}
	
	public void Load()
	{
		if(File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
			
			PlayerData data = (PlayerData)bf.Deserialize(file);
			file.Close();
			
			//Shows in console the location of where playerInfo.dat is saved.
			//Debug.Log(Application.persistentDataPath);
			
			health = data.getHealth();
			strength = data.getStrength();
			experience = data.getExperience();
			progress = data.getProgress();
		}
	}
}

[Serializable]
class PlayerData
{
	private int health;
	private int strength;
	private int experience;
	private bool[] progress;
	
	public void setHealth(int num)
	{
		health = num;
	}
	
	public int getHealth()
	{
		return health;
	}
	
	public void setStrength(int num)
	{
		strength = num;
	}
	
	public int getStrength()
	{
		return strength;
	}
	
	public void setExperience(int num)
	{
		experience = num;
	}
	
	public int getExperience()
	{
		return experience;
	}
	
	public void setProgress(bool[] array)
	{
		progress = array;
	}
	
	public bool[] getProgress()
	{
		return progress;
	}
}
