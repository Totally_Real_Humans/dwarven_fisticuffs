﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StoryButtonController : MonoBehaviour 
{
	
	public Image child;
	public string sceneName;
	private Button self;

	void Start () 
	{
		self = gameObject.GetComponent<Button>();
		if (self.interactable == false)
		{
			child.color = new Color32(41, 41, 41, 255);
		}
	}
	
	public void goToScene()
	{
			SceneManager.LoadScene(sceneName);
	}
	
}

