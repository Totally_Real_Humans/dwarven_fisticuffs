﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StoryController : MonoBehaviour 
{

	public GameObject[] spriteArray;
	public string[] storyArray;
	public TextAsset storyFile;
	public Text printLocation;
	public string nextScene;
	public int progressPoint;
	
	private float nextDelay = 0.25f;
	private float tillNext = 0;
	
	private int currentSpot = 0;
	
	public Button backButton;
	
	void Start () 
	{
		string story = storyFile.text;
		string[] splitter = new string[] {"\n"};
		storyArray = story.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
	}
	
	void Update () 
	{
		if (currentSpot == storyArray.Length)
		{
			changeScene();
		}
		else
		{
			printLocation.text = storyArray[currentSpot];
			
			if (currentSpot != 0)
			{
				if (spriteArray[currentSpot - 1] != spriteArray[currentSpot])
				{
					spriteArray[currentSpot - 1].SetActive(false);
				}
				backButton.interactable = true;
			}
			else
			{
				backButton.interactable = false;
			}
			
			if (spriteArray[currentSpot].activeSelf == false)
			{
				spriteArray[currentSpot].SetActive(true);
			}
			
			Control();
		}
	}
	
	void Control()
	{
		if (Time.fixedTime > tillNext)
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				currentSpot += 1;
				tillNext = Time.fixedTime + nextDelay;
			}
		
		}
	}
	
	public void Forward()
	{
		if (Time.fixedTime > tillNext)
		{
			currentSpot += 1;
			tillNext = Time.fixedTime + nextDelay;
		}

	}
	
	public void Skip ()
	{
		changeScene();
	}
	
	public void Back()
	{
		currentSpot = currentSpot - 1;
		spriteArray[currentSpot + 1].SetActive(false);
	}
	
	public void changeScene()
	{
		if (WorldController.control.progress[progressPoint] == false)
		{
			WorldController.control.progress[progressPoint] = true;
			WorldController.control.Save();
			SceneManager.LoadScene(nextScene);
		}
		else
		{
			SceneManager.LoadScene("PreviousStory");
		}
	}
}
