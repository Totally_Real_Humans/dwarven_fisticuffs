﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryTutorial : MonoBehaviour 
{

	public GameObject tapMessage;

	public bool tutorial = false;
	private float timeTillTutorial;

	void Start () 
	{
		if (WorldController.control.progress[0] == false)
		{
			timeTillTutorial = Time.fixedTime + 3.0f;
			tutorial = true;
		}
		
	}
	
	void Update () 
	{
		if (timeTillTutorial < Time.fixedTime && tutorial == true)
		{
			tapMessage.SetActive(true);
		}
	}
	
	public void turnOff()
	{
		if (tutorial == true)
		{
			tapMessage.SetActive(false);
			tutorial = false;
		}
	}
}
