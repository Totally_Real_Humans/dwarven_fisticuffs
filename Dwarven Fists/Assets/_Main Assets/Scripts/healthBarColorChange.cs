﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class healthBarColorChange : MonoBehaviour {
	public Slider slider;
	public float maxHealth;
	public Image bar;
	public float  healthRemaining;
	public int yellowChange;
	public int redChange;
	// Use this for initialization
	void Start () {
		maxHealth = slider.value;
	}
	
	// Update is called once per frame
	void Update () {
		healthRemaining=slider.value;
		if (healthRemaining < yellowChange && healthRemaining > redChange){
			bar.color=Color.yellow;
		}
		else if (healthRemaining < redChange){
			bar.color=Color.red;
		}
	}
}
