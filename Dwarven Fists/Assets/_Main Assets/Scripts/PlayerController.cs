﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour 
{
	private Animator animator;

	public float swipePixelAmount;
	public float dodgeSpeed;
	public float attackDistance;
	public float dodgeTime = 0.0f;
	public float attackTime = 0.0f;
	public float attackCooldown;
	public float dodgeCooldown;

	public BoxCollider2D rightFist;
	public BoxCollider2D leftFist;
	
	private bool dodgeOn = false;
	public bool attackOn = false;
	public bool headbuttOn = false;
	private bool rightFistOn = false;

	public Sprite sprite1;
	public Sprite sprite2;
	public Sprite sprite3;
	
	public Sprite dodgeSpriteLeft;
	public Sprite dodgeSpriteRight;
	public Sprite hitSprite;

	private SpriteRenderer dwarfSprite;
	public SpriteRenderer rightDwarfFist;
	public SpriteRenderer leftDwarfFist;
	
	private Vector3 initialPosition;

	public Slider playerHealth;
	public bool isDamaged = false;
	public bool isDamagedByLaser = false;
	
	public float hitReset = 0.0f;
	public float invulnerability;
	
	public bool isDefeated = false;
	
	public AudioSource oofSound;
	public AudioSource dodgeSound;
	public AudioSource punchingSound;
	
	public GameObject[] pauseObjects;
	
	public string state = "idle";
	private Vector2 touchPos;
	private Vector2 direction;
	private bool touchDone;
	public bool mobile = false;
	public bool hitFlag = false;
	public float damageTaken;
	
	public CameraShake cameraShake;
	public Text damagePopupText;
	public DwarfEvents dwarfEvents;
	
	public float stunDuration;
	
	public float totalDamageTaken;
	public Image playerHealthBar;
	
	
	void Start() 
	{
		dwarfSprite = GetComponent<SpriteRenderer>();
		initialPosition = transform.position;
		pauseObjects = GameObject.FindGameObjectsWithTag("Dwarf");
		animator = GetComponent<Animator>();
		dwarfEvents = this.GetComponent<DwarfEvents>();
		GameObject camera = GameObject.Find("Main Camera");
		cameraShake = camera.GetComponent<CameraShake>();
		
		//Setting Healthbar of player.
		playerHealth.maxValue = WorldController.control.health;
		playerHealth.value = WorldController.control.health;
	}

	void Update ()
	{
		StartCoroutine(stunned());
		StartCoroutine(blink());
		controls();
		dodge();
		headbutt();
		attack();
		healthLeft();
		damagePopupText.text = damageTaken.ToString();
		
		if (Input.GetKeyDown("["))
		{
			playerHealth.value = 0;
		}
	}
	
	void controls(){
		if (mobile == true){
			mobileControls();
		}
		else{
			computerControls();
		}
	}
	
	void OnTriggerEnter2D(Collider2D coll) 
	{
		if (coll.gameObject.tag == "Enemy Attack" && hitReset < Time.fixedTime && this.gameObject.tag == "Dwarf")
		{
			isDamaged = true;
		}
		if (coll.gameObject.tag =="iLaser" && hitReset < Time.fixedTime && this.gameObject.tag == "Dwarf"){
			isDamaged = true;
			isDamagedByLaser = true;
		}
		if (coll.gameObject.tag == "Brass Enemy"){
			isDamaged = true;
		}
	}

	void computerControls()
	{
		if (dodgeOn == false && attackOn == false)
		{
			if (Input.GetKeyDown("a") | Input.GetKeyDown("left") && state != "stunned")
			{
				state = "leftDodge";
				dodgeSound.pitch = 1.8f;
				dodgeSound.Play();
			}
			else if (Input.GetKeyDown("d") | Input.GetKeyDown("right") && state != "stunned")
			{
				state = "rightDodge";
				dodgeSound.pitch = 2.0f;
				dodgeSound.Play();
			}
			else if (Input.GetKeyDown(KeyCode.Space) && state != "stunned")
			{
				state = "punch";
				punchingSound.pitch = UnityEngine.Random.Range(1.2f, 1.6f);
				punchingSound.Play();
			}
			else if (Input.GetKeyDown("w") | Input.GetKeyDown("up"))
			{
				state = "headbutt";
			}
		}
		else
		{
			if (state != "stunned")
			{
				state = "idle";
			}
		}
		
	}
	
	void mobileControls()
	{
		if (Input.touchCount > 0)
        {
			Touch touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    touchPos = touch.position;
					direction = touch.position - touchPos;
                    break;

                case TouchPhase.Moved:
                    direction = touch.position - touchPos;
					if (Math.Abs(direction.x) > swipePixelAmount  || direction.y > swipePixelAmount * 2)
					{
						touchDone = true;
					}
                    break;

                case TouchPhase.Ended:
					touchDone = true;
                    break;
            }
        }
		
		if (dodgeOn == false && attackOn == false && touchDone == true)
		{
			float headbuttOrDodge = Math.Abs(direction.x) - Math.Abs(direction.y * 2);
			if (direction.y > (swipePixelAmount * 2)  && headbuttOrDodge < 40)
			{
				state = "headbutt";
			}
			else if (direction.x < -swipePixelAmount)
			{
				state = "leftDodge";
			}
			else if (direction.x > swipePixelAmount)
			{
				state = "rightDodge";
			}
			else
			{
				state = "punch";
			}
		}
		else
		{
			state = "idle";
		}
		
		touchDone = false;
	}
	
	void dodge()
	{
		if (state == "leftDodge")
		{
			dwarfSprite.sprite = dodgeSpriteLeft;
			dodgeTime = Time.fixedTime + dodgeCooldown;
			dodgeSpeed =  - (Math.Abs(dodgeSpeed));
			dodgeOn = true;
		}
		if (state == "rightDodge")
		{
			dwarfSprite.sprite = dodgeSpriteRight;
			dodgeTime = Time.fixedTime + dodgeCooldown;
			dodgeSpeed =  Math.Abs(dodgeSpeed);
			dodgeOn = true;
		}

		if (dodgeOn == true)
		{
			if (Time.fixedTime < dodgeTime - (2 * dodgeCooldown)/3)
			{
				transform.position += new Vector3(dodgeSpeed, 0, 0);
			}
			else if (Time.fixedTime < dodgeTime - (dodgeCooldown/ 3))
			{
				transform.position += new Vector3(0, 0, 0);
			}
			else if (Time.fixedTime < dodgeTime)
			{
				transform.position += new Vector3(-dodgeSpeed, 0, 0);
			}
			else
			{
				transform.position = initialPosition;
				dodgeOn = false;
				if (state != "stunned")
					dwarfSprite.sprite = sprite1;
			}
		}
	}

	void attack()
	{
		if (state == "punch")
		{
			attackTime = Time.fixedTime + attackCooldown;
			attackOn = true;
		}

		if (attackOn == true && state != "stunned" && headbuttOn == false)
		{
			if (Time.fixedTime < attackTime - (attackCooldown/2))
			{
				if (rightFistOn == false){
					dwarfSprite.sprite = sprite2;
					rightFist.enabled = true;
					rightDwarfFist.enabled = true;
					transform.position += new Vector3(-attackDistance * 2, attackDistance, 0);
				}
				else 
				{
					dwarfSprite.sprite = sprite3;
					leftFist.enabled = true;
					leftDwarfFist.enabled = true;
					transform.position += new Vector3(attackDistance * 2, attackDistance, 0);
				}
				
				//transform.position += new Vector3(attackDistance, attackDistance, 0);
			}
			else if (Time.fixedTime < attackTime)
			{
				dwarfSprite.sprite = sprite1;
				rightFist.enabled = false;
				rightDwarfFist.enabled = false;
				leftFist.enabled = false;
				leftDwarfFist.enabled = false;
				transform.position += new Vector3(0, -attackDistance, 0);
			}
			else
			{
				transform.position = initialPosition;
				attackOn = false;
				if (rightFistOn == false){
					rightFistOn = true;
				}
				else 
				{
					rightFistOn = false;
				}
			}
		}
		
		else if (state == "stunned")
		{
			attackOn = false;
			rightFist.enabled = false;
			rightDwarfFist.enabled = false;
			leftFist.enabled = false;
			leftDwarfFist.enabled = false;
		}
	}
	
	void headbutt()
	{
		if (state == "headbutt")
		{
			animator.enabled = true;
			attackOn = true;
			headbuttOn = true;
		}
		if (attackOn == true && state != "stunned" && headbuttOn == true)
		{
			animator.SetTrigger("dwarfHeadbutt");
		}
		else if (attackOn == false && headbuttOn == false)
		{
			animator.enabled = false;
		}
	}
	
	void healthLeft()
	{
		if(isDamaged == true)
		{
			blink();
			stunned();
			cameraShake.shakeDuration = 0.1f;
			transform.position = initialPosition;
			oofSound.Play();
			playerHealth.value = playerHealth.value - EnemyController.enemyStrength;
			damageTaken = EnemyController.enemyStrength;
			dwarfEvents.textOn(0);
			if (playerHealth.value == 0)
			{
				defeatConditions();
			}
			totalDamageTaken += damageTaken;
			playerHealthBar.fillAmount = (playerHealth.maxValue - totalDamageTaken)/playerHealth.maxValue;
			isDamaged = false;
			isDamagedByLaser = false;
			hitReset = Time.fixedTime + invulnerability;
		}
	}
	
	public IEnumerator stunned(){
		if (isDamaged == true){
			state = "stunned";
			dwarfSprite.sprite = hitSprite;
			if (headbuttOn == true){
				yield return new WaitForSeconds(stunDuration * 2);
			}
			else{
				yield return new WaitForSeconds(stunDuration);
			}
			state = "idle";
			dwarfSprite.sprite = sprite1;
			dwarfEvents.textOff(0);
		}
	}
	
	public IEnumerator blink(){
		if (isDamaged == true){
			Color tmp = dwarfSprite.color;
			tmp.a = 0f;
			dwarfSprite.color = tmp;
			yield return new WaitForSeconds (0.10f);
			tmp.a = 255f;
			dwarfSprite.color = tmp;
			yield return new WaitForSeconds (0.10f);
			tmp.a = 0f;
			dwarfSprite.color = tmp;
			yield return new WaitForSeconds (0.10f);
			tmp.a = 255f;
			dwarfSprite.color = tmp;
			yield return new WaitForSeconds (0.10f);
			tmp.a = 255f;
			dwarfSprite.color = tmp;
		}
	}

	void defeatConditions()
	{
		foreach (GameObject obj in pauseObjects)
		{
				obj.SetActive(false);
		}
		isDefeated = true;
	}
	
	public bool isDefeatedQuestion()
	{
		return isDefeated;
	}
}
