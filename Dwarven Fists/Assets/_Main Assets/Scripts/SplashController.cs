﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashController : MonoBehaviour {

	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
		{
			gotoMainMenu();
		}
	}
	
	public void gotoMainMenu()
	{
		SceneManager.LoadScene("Main Menu");
	}
}
