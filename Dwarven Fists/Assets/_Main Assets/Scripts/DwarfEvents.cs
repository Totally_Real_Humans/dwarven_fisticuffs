﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DwarfEvents : MonoBehaviour {

	public bool[] textboxStatus;
	public GameObject[] textboxes;
	
	private int randomNum;
	private int range;
	private float chance;
	
	public void textOn(int x)
	{
		if (textboxStatus[x] == false)
		{
			textboxes[x].SetActive(true);
		}
	}
	
	public void textOffForFight(int x)
	{
		textboxes[x].SetActive(false);
		textboxStatus[x] = true;
	}
	
	public void textOff(int x)
	{
		textboxes[x].SetActive(false);
	}
	
	public void setRange(int x)
	{
		range = x;
	}
	
	public void setChance(float y)
	{
		chance = y;
	}
	
	public void textOnRandom(int starting)
	{
		randomNum = Random.Range(starting, starting + range);
		float randomFloat = Random.Range(0.0f, 100.0f);
		if (randomFloat < chance)
		{
			textboxes[randomNum].SetActive(true);
		}
	}
	
	public void textOffRandom()
	{
		textboxes[randomNum].SetActive(false);
	}
}
