﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PickAFight : MonoBehaviour {


	public GameObject children;
	public GameObject upgradeButton;
	public GameObject previousStory;
	
	public Button moonLockButton;
	public Button iGorButton;
	
	public Image tutorialImage;
	public Sprite tutorialMessage;
	
	
	void Start()
	{
		//Code Commented out here till we are ready to have players start from beginning.
		//if (WorldController.control.progress[3] == true)
		//{			
			moonLockButton.interactable = true;
		//}
		//if (WorldController.control.progress[6] == true)
		//{

			iGorButton.interactable = true;
		//}
	}
	
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			TurnOnPickAFight();
		}

	}
	
	public void TurnOnPickAFight()
	{
		if (WorldController.control.progress[0] == false)
		{
			tutorialImage.sprite = tutorialMessage;
		}
		children.SetActive(true);
		upgradeButton.SetActive(false);
		previousStory.SetActive(false);
	}
	
	public void TurnOffPickAFight()
	{
		children.SetActive(false);
		//These are likely no longer needed due to the new UI.
		//upgradeButton.SetActive(true);
		//previousStory.SetActive(true);

	}
}
