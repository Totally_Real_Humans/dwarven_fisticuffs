﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarLabel : MonoBehaviour {
	
	public Slider healthBar;
	public Text healthLeft;
	public string entityName;
	
	void Start () 
	{
		entityName = healthLeft.text;
		
	}
	
	void Update () 
	{
		healthLeft.text = entityName + "    " + healthBar.value;
	}
	
	
}
