﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PreviousStoryController : MonoBehaviour 
{

	public Button[] buttonArray;

	void Awake () 
	{
		for (int num = 1; num < WorldController.control.progress.Length; num++)
		{
			if (WorldController.control.progress[num] == true)
			{
				buttonArray[num - 1].interactable = true;
			}
		}
	}
	
	public void backToMainMenu()
	{
		SceneManager.LoadScene("Main Menu");
	}

}
