﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ResultsController : MonoBehaviour 
{
	public GameObject victory;
	public GameObject defeat;
	public SpriteRenderer background;
	
	public Text experienceNumber;
	public Text totalExperienceNumber;

	private int winQuestion;
	private string enemy;
	
	public float scoreSpeed;
	
	private float nextScoreIncrease;
	private int experienceIncrease;
	private int textExperience = 0;

	void Start () 
	{
		winQuestion = PlayerPrefs.GetInt("Win");
		Debug.Log(winQuestion);
		enemy = PlayerPrefs.GetString("Enemy");
		Debug.Log(enemy);
		PlayerPrefs.DeleteAll();
		
		experienceGain();
		totalExperienceNumber.text = WorldController.control.experience.ToString();
		WorldController.control.experience += experienceIncrease;
		WorldController.control.Save();
		Debug.Log(WorldController.control.experience);
		Debug.Log(experienceIncrease);
		nextScoreIncrease = Time.fixedTime + 0.3f;
	}
	
	void Update () 
	{
		if (textExperience < experienceIncrease)
		{
			if (nextScoreIncrease < Time.fixedTime)
			{
				textExperience += 5;
			}
		}
		experienceNumber.text = textExperience.ToString();
		if (textExperience == experienceIncrease)
		{
			totalExperienceNumber.text = WorldController.control.experience.ToString();
		}
	}
	
	void experienceGain()
	{
		if (enemy == "Brass Roe")
		{
			experienceIncrease = 25;
		}
		else if (enemy == "Moonlock")
		{
			experienceIncrease = 50;
		}
		else if (enemy  == "iGor")
		{
			experienceIncrease = 100;
		}
		
		if (winQuestion == 1)
		{
			experienceIncrease = experienceIncrease * 4;
			victory.SetActive(true);
		}
		else
		{
			defeat.SetActive(true);
			background.color = Color.gray;
		}
	}
	
	public void replayLevel()
	{
		SceneManager.LoadScene(enemy +" Fight");
	}
	
	public void mainMenu()
	{
		SceneManager.LoadScene("Main Menu");
	}
}
