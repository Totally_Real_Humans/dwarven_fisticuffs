﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UpgradeController : MonoBehaviour 
{
	public float xpCostGrowth;

	public Button healthButton;
	public Text healthXPCost;
	public Button strengthButton;
	public Text strengthXPCost;
	public Text totalExperience;
	
	public Button saveButton;
	public GameObject resetProgressPopup;
	
	private Text healthText;
	private Text strengthText;
	
	private int healthCopy;
	private int healthXP;
	private int strengthCopy;
	private int strengthXP;
	private int experienceCopy;

	void Start() 
	{
		experienceCopy = WorldController.control.experience;
		healthInfo();
		strengthInfo();
		updateInfo();
	}
	
	private void healthInfo()
	{
		healthText = healthButton.GetComponentInChildren<Text>();
		healthCopy = WorldController.control.health;
		healthText.text = healthCopy.ToString();
		
	}
	
	private void strengthInfo()
	{
		strengthText = strengthButton.GetComponentInChildren<Text>();
		strengthCopy = WorldController.control.strength;
		strengthText.text = strengthCopy.ToString();
	}
	
	private void updateInfo()
	{
		totalExperience.text = experienceCopy.ToString();
		healthXP = setXPCost(healthCopy/10);
		healthXPCost.text = healthXP.ToString();
		if (experienceCopy < healthXP)
		{
			LockButton(healthButton);
		}
		else
		{
			UnlockButton(healthButton);
		}
		
		strengthXP = setXPCost(strengthCopy);
		strengthXPCost.text = strengthXP.ToString();
		if (experienceCopy < strengthXP)
		{
			LockButton(strengthButton);
		}
		else
		{
			UnlockButton(strengthButton);
		}
	}
	
	int setXPCost(int stat)
	{
		float xp = 100.0f;
		stat -= 10;
		for (int i = 0; i < stat; ++i)
		{
			xp *= xpCostGrowth;
		}
		return (int)xp;
	}
	
	public void UpgradeHealth()
	{
		healthCopy += 10;
		healthText.text = healthCopy.ToString();
		experienceCopy -= healthXP;
		updateInfo();
		UnlockButton(saveButton);
	}
	
	public void UpgradeStrength()
	{
		strengthCopy += 1;
		strengthText.text = strengthCopy.ToString();
		experienceCopy -= strengthXP;
		updateInfo();
		UnlockButton(saveButton);
	}
	
	public void SetStats()
	{
		WorldController.control.health = healthCopy;
		WorldController.control.strength = strengthCopy;
		WorldController.control.experience = experienceCopy;
		WorldController.control.Save();
		LockButton(saveButton);
	}
	
	public void UnlockButton(Button button)
	{
		Selectable isSelectable = button.GetComponent<Selectable>();
		Image imageColor = button.GetComponent<Image>();
		isSelectable.enabled = true;
		imageColor.color = Color.white;
	}
	
	public void LockButton(Button button)
	{
		Selectable isSelectable = button.GetComponent<Selectable>();
		Image imageColor = button.GetComponent<Image>();
		isSelectable.enabled = false;
		imageColor.color = Color.grey;
	}
	
	public void ResetXPSpent()
	{
		experienceCopy = WorldController.control.experience;
		healthInfo();
		strengthInfo();
		updateInfo();
		LockButton(saveButton);
	}
	
	public void ResetProgress()
	{
		bool[] progressReset = new bool[]{false, false, false, false, false, false, false, false, false, false};
		healthCopy = 100;
		strengthCopy = 10;
		experienceCopy = 0;
		SetStats();
		WorldController.control.progress = progressReset;
		WorldController.control.Save();
		SceneManager.LoadScene("Character Screen");
		SceneManager.LoadScene("Splash");
	}
	
	public void activateRestPopup()
	{
		resetProgressPopup.SetActive(true);
	}
	
	public void no()
	{
		resetProgressPopup.SetActive(false);
	}
	
	public void yes()
	{
		ResetProgress();
	}
	
	public void MainMenu()
	{
		SceneManager.LoadScene("Main Menu");
	}
}
